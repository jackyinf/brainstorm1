﻿using System.Collections;
using System.Collections.Generic;
using UniRx;
using UniRx.Triggers;
using UnityEngine;
using UnityEngine.Assertions;

public class TestScene_Player_Movement : MonoBehaviour
{
    private float speed = 100f;
    private Rigidbody2D rb;
    private TestScene_Player_GroundCheck groundCheck;

    public float jumpForce = 100f;

    public void Awake()
    {
        groundCheck = GetComponentInChildren<TestScene_Player_GroundCheck>();
        Assert.IsNotNull(groundCheck);

        rb = GetComponent<Rigidbody2D>();
        Assert.IsNotNull(rb);
    }

    public void Start()
    {
        this.OnCollisionEnter2DAsObservable()
            .Subscribe(collision2D =>
            {
                Debug.Log($"Collided with {collision2D}");
            });
    }

    public void Update()
    {
        var movement = new Vector3(Input.GetAxis("Horizontal"), 0);
        transform.position += movement * Time.deltaTime * speed;

        if (groundCheck.Grounded.Value && Input.GetKeyDown(KeyCode.Space))
            rb.AddForce(Vector2.up * jumpForce, ForceMode2D.Impulse);
    }
}
