﻿using System.Collections;
using System.Collections.Generic;
using UniRx;
using UniRx.Triggers;
using UnityEngine;

public class TestScene_Player_GroundCheck : MonoBehaviour
{
    public IReactiveProperty<bool> Grounded { get; } = new ReactiveProperty<bool>(true);

    public void Start()
    {
        Grounded.Subscribe(isGrounded => Debug.Log($"Is grounded - {isGrounded}"));

        this.OnTriggerEnter2DAsObservable()
            .Where(x => x.gameObject.CompareTag("Ground"))
            .Subscribe(_ => Grounded.Value = true);

        this.OnTriggerExit2DAsObservable()
            .Where(x => x.gameObject.CompareTag("Ground"))
            .Subscribe(_ => Grounded.Value = false);
    }
}
