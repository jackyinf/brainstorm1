﻿namespace Assets._Game01._Scripts.Utils
{
    public static class Archer_Constants
    {
        public static class Tags
        {
            public const string Wall = "Wall";
            public const string Enemy = "Enemy";
        }
    }
}