﻿using UniRx;
using UnityEngine;

public class Archer_Enemy : MonoBehaviour
{
    public IReactiveProperty<float> Hp { get; } = new ReactiveProperty<float>(10);

    public Archer_Enemy_Movement enemyMovement;
    public Archer_Enemy_Attack enemyAttack;

    public void Awake ()
    {
        enemyMovement = GetComponent<Archer_Enemy_Movement>();
        enemyAttack = GetComponent<Archer_Enemy_Attack>();

        enemyAttack.IsAttackingWall.Subscribe(isAttacking => enemyMovement.isMoving = !isAttacking);
    }
}
