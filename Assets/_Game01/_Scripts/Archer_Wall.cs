﻿using UniRx;
using UnityEngine;

namespace Assets._Game01._Scripts
{
    public class Archer_Wall : MonoBehaviour
    {
        public IReactiveProperty<float> Hp { get; } = new ReactiveProperty<float>(30);
        public void TakeDamage(float damage) => Hp.Value -= damage;

        public void Start()
        {
            Hp.Subscribe(hp =>
            {
                if (hp <= 0)
                    Die();
            });
        }

        private void Die() => Destroy(gameObject);
    }
}