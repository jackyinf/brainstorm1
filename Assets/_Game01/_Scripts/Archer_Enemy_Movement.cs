﻿using UnityEngine;

[RequireComponent(typeof(Archer_Enemy))]
public class Archer_Enemy_Movement : MonoBehaviour
{
    private readonly Vector3 _moveDirection = new Vector3(-1.0f, 0.0f, 0.0f);

    public float speed = 10f;
    public bool isMoving = true;

    public void FixedUpdate()
    {
        if (isMoving)
            transform.position += _moveDirection * speed * Time.deltaTime;
    }
}
