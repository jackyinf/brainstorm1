﻿using Assets._Game01._Scripts.Utils;
using UniRx;
using UnityEngine;

[RequireComponent(typeof(Archer_Enemy))]
public class Archer_Enemy_Attack : MonoBehaviour
{
    public IReactiveProperty<bool> IsAttackingWall { get; } = new ReactiveProperty<bool>(false);

    public readonly float defaultCooldown = 1f;
    public float currentCooldown;
    public float currentHp = 10f;
    public float damage = 10f;
    
    public delegate void WallAttack(float damage);
    public event WallAttack OnWallAttack;

    public void Start()
    {
        currentCooldown = defaultCooldown;
    }

    public void FixedUpdate()
    {
        if (currentCooldown > 0)
            currentCooldown -= Time.fixedDeltaTime;

        if (IsAttackingWall.Value && currentCooldown <= 0)
        {
            OnWallAttack?.Invoke(damage);
            currentCooldown = defaultCooldown;
        }
    }

    public void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.gameObject.CompareTag(Archer_Constants.Tags.Wall))
            IsAttackingWall.Value = true;
    }

    public void OnTriggerExit2D(Collider2D collider)
    {
        if (collider.gameObject.CompareTag(Archer_Constants.Tags.Wall))
            IsAttackingWall.Value = false;
    }
}
