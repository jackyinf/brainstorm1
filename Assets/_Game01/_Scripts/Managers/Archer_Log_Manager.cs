﻿using Assets._Game01._Scripts.Utils;
using UniRx;
using UnityEngine;
using UnityEngine.Assertions;

namespace Assets._Game01._Scripts.Managers
{
    public class Archer_Log_Manager : MonoBehaviour
    {
        public void Awake()
        {
            var wall = GameObject.FindGameObjectWithTag(Archer_Constants.Tags.Wall)?.GetComponent<Archer_Wall>();
            Assert.IsNotNull(wall);
            // ReSharper disable once PossibleNullReferenceException
            wall.Hp.Subscribe(hp => Debug.Log($"Hp left: {hp}"));

            var enemyManager = GetComponent<Archer_Enemy_Manager>();
            Assert.IsNotNull(enemyManager);

            enemyManager.enemies.ObserveAdd().Subscribe(_ => Debug.Log("Enemy has been added"));
            enemyManager.enemies.ObserveRemove().Subscribe(_ => Debug.Log("Enemy has been removed"));
        }
    }
}