﻿using System.Linq;
using Assets._Game01._Scripts.Utils;
using UniRx;
using UniRx.Triggers;
using UnityEngine;
using UnityEngine.Assertions;

namespace Assets._Game01._Scripts.Managers
{
    public class Archer_Enemy_Manager : MonoBehaviour
    {
        public ReactiveCollection<Archer_Enemy> enemies = new ReactiveCollection<Archer_Enemy>();
        private Archer_Wall _wall;

        public void Awake()
        {
            _wall = GameObject.FindGameObjectWithTag(Archer_Constants.Tags.Wall)?.GetComponent<Archer_Wall>();
            Assert.IsNotNull(_wall);

            enemies.ObserveAdd()
                .Select(enemyEvent => enemyEvent.Value)
                .Subscribe(enemy =>
                {
                    enemy.enemyAttack.IsAttackingWall.Subscribe(isAttacking =>
                    {
                        if (isAttacking)
                            enemy.enemyAttack.OnWallAttack += GiveDamageToWall;
                        else
                            enemy.enemyAttack.OnWallAttack -= GiveDamageToWall;
                    });
                    enemy.gameObject.OnDestroyAsObservable().Subscribe(_ => enemies.Remove(enemy));
                });

            enemies.ObserveRemove()
                .Select(enemyEvent => enemyEvent.Value)
                .Subscribe(enemy => enemy.enemyAttack.OnWallAttack -= GiveDamageToWall);
        }

        public void Start()
        {
            var enemiesList = GameObject.FindGameObjectsWithTag(Archer_Constants.Tags.Enemy)?.Select(x => x.GetComponent<Archer_Enemy>()).ToList();
            Assert.IsNotNull(enemiesList, "Failed to get existing enemies");
            // ReSharper disable once AssignNullToNotNullAttribute
            Assert.IsTrue(enemiesList.All(x => x != null), $"Some items do not have {nameof(Archer_Enemy)} script attached");

            foreach (var enemy in enemiesList)
                enemies.Add(enemy);
        }

        private void GiveDamageToWall(float damage) => _wall.TakeDamage(damage);
    }
}