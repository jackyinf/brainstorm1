﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

// this is the master game script, attached to th ebow

public class bowAndArrow : MonoBehaviour
{
	
	// to determine the mouse position, we need a raycast
	private Ray mouseRay1;
	private RaycastHit rayHit;
	// position of the raycast on the screen
	private float posX;
	private float posY;

	// References to the gameobjects / prefabs
	public GameObject bowString;
	GameObject arrow;
	public GameObject arrowPrefab;
	//public GameObject gameManager;	
	public GameObject risingText;
	//public GameObject target;

	// Sound effects
	public AudioClip stringPull;
	public AudioClip stringRelease;
	public AudioClip arrowSwoosh;

	// has sound already be played
	bool stringPullSoundPlayed;
	bool stringReleaseSoundPlayed;
	bool arrowSwooshSoundPlayed;

	// the bowstring is a line renderer
	private List<Vector3> bowStringPosition;
	LineRenderer bowStringLinerenderer;

	// to determine the string pullout
	float arrowStartX;
	float length;

	// some status vars
	bool arrowShot;
	bool arrowPrepared;

	// position of the line renderers middle part 
	Vector3 _stringPullout;

    readonly Vector3 _stringRestPosition = new Vector3 (-0.44f, -0.06f, 2f);

	// references to main objects for the UI screens

	// amount of arrows for the game
	public int arrows = 20;
	// actual score
	public int score = 0;

	// Use this for initialization
	void Start () {
		// create an arrow to shoot
		// use true to set the target
		CreateArrow (true);

		// setup the line renderer representing the bowstring
		bowStringLinerenderer = bowString.AddComponent<LineRenderer>();
		bowStringLinerenderer.SetVertexCount(3);
		bowStringLinerenderer.SetWidth(0.05F, 0.05F);
		bowStringLinerenderer.useWorldSpace = false;
		bowStringLinerenderer.material = Resources.Load ("Materials/bowStringMaterial") as Material;
		bowStringPosition = new List<Vector3> ();
		bowStringPosition.Add(new Vector3 (-0.44f, 1.43f, 2f));
		bowStringPosition.Add(new Vector3 (-0.44f, -0.06f, 2f));
		bowStringPosition.Add(new Vector3 (-0.43f, -1.32f, 2f));
		bowStringLinerenderer.SetPosition (0, bowStringPosition [0]);
		bowStringLinerenderer.SetPosition (1, bowStringPosition [1]);
		bowStringLinerenderer.SetPosition (2, bowStringPosition [2]);
		arrowStartX = 0.7f;

		_stringPullout = _stringRestPosition;
	}
    
	// Update is called once per frame
    void Update()
    {
        // check the game states

        // game is steered via mouse
        // (also works with touch on android)
        if (Input.GetMouseButton(0))
        {
            // the player pulls the string
            if (!stringPullSoundPlayed)
            {
                // play sound
                GetComponent<AudioSource>().PlayOneShot(stringPull);
                stringPullSoundPlayed = true;
            }
            // detrmine the pullout and set up the arrow
            PrepareArrow();
        }

        // ok, player released the mouse
        // (player released the touch on android)
        if (Input.GetMouseButtonUp(0) && arrowPrepared)
        {
            // play string sound
            if (!stringReleaseSoundPlayed)
            {
                GetComponent<AudioSource>().PlayOneShot(stringRelease);
                stringReleaseSoundPlayed = true;
            }
            // play arrow sound
            if (!arrowSwooshSoundPlayed)
            {
                GetComponent<AudioSource>().PlayOneShot(arrowSwoosh);
                arrowSwooshSoundPlayed = true;
            }
            // shot the arrow (rigid body physics)
            ShootArrow();
        }
        // in any case: update the bowstring line renderer
        DrawBowString();
    }

    public void CreateArrow(bool hitTarget)
    {
        // when a new arrow is created means that:
        // sounds has been played
        stringPullSoundPlayed = false;
        stringReleaseSoundPlayed = false;
        arrowSwooshSoundPlayed = false;

        // does the player has an arrow left ?
        if (arrows > 0)
        {
            // now instantiate a new arrow
            this.transform.localRotation = Quaternion.identity;
            arrow = Instantiate(arrowPrefab, Vector3.zero, Quaternion.identity) as GameObject;
            arrow.name = "arrow";
            arrow.transform.localScale = this.transform.localScale;
            arrow.transform.localPosition = this.transform.position + new Vector3(0.7f, 0, 0);
            arrow.transform.localRotation = this.transform.localRotation;
            arrow.transform.parent = this.transform;

            // transmit a reference to the arrow script
            //arrow.GetComponent<rotateArrow>().setBow(gameObject);
            arrowShot = false;
            arrowPrepared = false;

            // subtract one arrow
            arrows--;
        }
    }

    public void PrepareArrow()
    {
        // get the touch point on the screen
        mouseRay1 = Camera.main.ScreenPointToRay(Input.mousePosition);
        if (Physics.Raycast(mouseRay1, out rayHit, 1000f) && arrowShot == false)
        {
            // determine the position on the screen
            posX = this.rayHit.point.x;
            posY = this.rayHit.point.y;

            // set the bows angle to the arrow
            Vector2 mousePos = new Vector2(transform.position.x - posX, transform.position.y - posY);
            float angleZ = Mathf.Atan2(mousePos.y, mousePos.x) * Mathf.Rad2Deg;
            transform.eulerAngles = new Vector3(0, 0, angleZ);

            // determine the arrow pullout
            length = mousePos.magnitude / 3f;
            length = Mathf.Clamp(length, 0, 1);

            // set the bowstrings line renderer
            _stringPullout = new Vector3(-(0.44f + length), -0.06f, 2f);

            // set the arrows position
            Vector3 arrowPosition = arrow.transform.localPosition;
            arrowPosition.x = (arrowStartX - length);
            arrow.transform.localPosition = arrowPosition;
        }
        arrowPrepared = true;
    }

    public void ShootArrow()
    {
        if (arrow.GetComponent<Rigidbody>() == null)
        {
            arrowShot = true;
            var arrowRigidbody = arrow.AddComponent<Rigidbody>();
            arrow.transform.parent = null;
            arrowRigidbody.AddForce(
                Quaternion.Euler(new Vector3(transform.rotation.eulerAngles.x, transform.rotation.eulerAngles.y,
                    transform.rotation.eulerAngles.z)) * new Vector3(25f * length, 0, 0), ForceMode.VelocityChange);
        }
        arrowPrepared = false;
        _stringPullout = _stringRestPosition;
        CreateArrow(false);
    }

    public void DrawBowString()
    {
        if (bowStringPosition == null || bowStringPosition.Count <= 0)
            return;
        bowStringLinerenderer = bowString.GetComponent<LineRenderer>();
        bowStringLinerenderer.SetPosition(0, bowStringPosition[0]);
        bowStringLinerenderer.SetPosition(1, _stringPullout);
        bowStringLinerenderer.SetPosition(2, bowStringPosition[2]);
    }
}
