﻿using UnityEngine;
using System.Collections;
using Assets._Game01._Scripts.Utils;

public class rotateArrow : MonoBehaviour
{
    bool collisionOccurred;
    public GameObject arrowHead;
    public AudioClip targetHit;

    // the vars realize the fading out of the arrow when target is hit
    float alpha;
    float life_loss;
    public Color color = Color.white;
    private Renderer someRenderer;
    private AudioSource someAudioSource;
    private Rigidbody someRigidbody;

    public void Awake()
    {
        someRenderer = GetComponent<Renderer>();
        someAudioSource = GetComponent<AudioSource>();
        someRigidbody = GetComponent<Rigidbody>();
    }

    public void Start()
    {
        // set the initialization values for fading out
        float duration = 2f;
        life_loss = 1f / duration;
        alpha = 1f;
    }

    // Update is called once per frame
    public void FixedUpdate()
    {
        //this part of update is only executed, if a rigidbody is present
        // the rigidbody is added when the arrow is shot (released from the bowstring)
        var rb = transform.GetComponent<Rigidbody>();
        if (rb != null)
        {
            // do we fly actually?
            if (rb.velocity != Vector3.zero)
            {
                // get the actual velocity
                Vector3 vel = rb.velocity;
                // calc the rotation from x and y velocity via a simple atan2
                float angleZ = Mathf.Atan2(vel.y, vel.x) * Mathf.Rad2Deg;
                float angleY = Mathf.Atan2(vel.z, vel.x) * Mathf.Rad2Deg;
                // rotate the arrow according to the trajectory
                transform.eulerAngles = new Vector3(0, -angleY, angleZ);
            }
        }

        //// if the arrow hit something...
        //if (collisionOccurred)
        //{
        //    // fade the arrow out
        //    alpha -= Time.deltaTime * life_loss;
        //    if (someRenderer != null)
        //        someRenderer.material.color = new Color(color.r, color.g, color.b, alpha);

        //    // if completely faded out, die:
        //    if (alpha <= 0f)
        //        Destroy(gameObject);
        //}
    }
    
    public void OnCollisionEnter2D(Collision2D other)
    {
        //so, did a collision occur already?
        if (collisionOccurred)
        {
            transform.position = new Vector3(other.transform.position.x, transform.position.y, transform.position.z); // fix the arrow and let it not move anymore
            return; // the rest of the method hasn't to be calculated
        }
        
        Debug.Log("Collision!");
        if (other.gameObject.CompareTag(Archer_Constants.Tags.Enemy))
        {
            someAudioSource.PlayOneShot(targetHit); // play the audio file ("trrrrr")
            someRigidbody.velocity = Vector3.zero; // set velocity to zero
            someRigidbody.isKinematic = true; // disable the rigidbody
            someRigidbody.constraints = RigidbodyConstraints.FreezeAll;
            collisionOccurred = true; // and a collision occurred
            arrowHead.SetActive(false); // disable the arrow head to create optical illusion that arrow hit the target
        }
    }
}
